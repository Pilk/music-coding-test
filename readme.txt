Coding Exercise

Produce a C++ template FIFO class for a Linux based system which can be used to pass work items (For example, a struct defining some work to be done) from a high priority, timing critical thread (the writer) to a low-priority background thread (the reader) where the work is done.

The class should be able to queue a number of objects in a thread-safe manner for a single reader & writer.

The class should provide the following functions :

1) pop
	The reader thread calls this function to fetch the next available item. If no items are available, the background thread should be put to sleep until an item becomes available.
	
2) pop_try
	The reader thread calls this function to fetch the next available item. If no items are available, the function will return immediately and **indicate** to the calling thread that no items are available.

3) push
	The writer thread calls this function to push an item into the queue. If there is no room in the queue for the item, the function should return immediately and **indicate** to the calling thread that the item was not pushed to the queue.
	
The code should be submitted in the form of a git repository and ideally it should be possible to build
and run the code using GCC on a Linux based system. You can make use of 3rd party libraries such
as STL or Boost.