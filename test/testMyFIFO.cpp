#define BOOST_TEST_MODULE
#include <boost/test/unit_test.hpp>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>

#include "MyFIFO/MyFIFO.h"
#define SIZE_OF_QUEUE 5

BOOST_AUTO_TEST_CASE(ShouldPushToQueue) {
	MyFIFO <int> fifo = MyFIFO <int> ();
	
	fifo.push(1);
	fifo.push(2);
	fifo.push(3);
	
	assert(1 == fifo.pop());
	assert(2 == fifo.pop());
	assert(3 == fifo.pop());
	fifo.dump();

}

BOOST_AUTO_TEST_CASE(ShouldWrapAround) {
	MyFIFO <int> fifo = MyFIFO <int> ();
	
	for(int i=0; i<SIZE_OF_QUEUE; i++){
		fifo.push(i);
		assert(i == fifo.pop());
	}
	
	fifo.push(8);
	assert(8 == fifo.pop());
	assert(8 == fifo.get(0));
	fifo.dump();
}

BOOST_AUTO_TEST_CASE(PopTry_HasItems_ShouldPop) {
	MyFIFO <int> fifo = MyFIFO <int> ();

	fifo.push(1);
	
	try {
		fifo.pop_try();
	} catch(MyFIFOException e) {
		assert(false);
	}
}

BOOST_AUTO_TEST_CASE(PopTry_NoItems_ShouldThrow) {
	MyFIFO <int> fifo = MyFIFO <int> ();

	bool exceptionThrown = false;
	try {
		fifo.pop_try();
	} catch(MyFIFOException e) {
		exceptionThrown = !exceptionThrown;
	}
	assert(exceptionThrown);
}

BOOST_AUTO_TEST_CASE(Push_TooManyItems_ShouldThrow) {
	MyFIFO <int> fifo = MyFIFO <int> ();

	bool exceptionThrown = false;
	try {
		for(int i=0; i<SIZE_OF_QUEUE + 1; i++) {
			fifo.push(i);
		}
	} catch(MyFIFOException e) {
		exceptionThrown = !exceptionThrown;
	}
	assert(exceptionThrown);
	assert(fifo.get(0) != SIZE_OF_QUEUE + 1);
}

class Reader {

	MyFIFO<int> * m_fifo;
	boost::thread m_thread;

	void work() {
		for(int i = 0; i < SIZE_OF_QUEUE; i++) {
			m_thread.yield();
			int item = m_fifo->pop();
			printf("%d/", item);
		}
	}
	
	public:
	Reader(MyFIFO<int> * fifo) {
		m_fifo = fifo;
		boost::thread m_thread(&Reader::work, this);
	};
	
	void start() {
		 m_thread.detach();
	}
};

class Writer {

	MyFIFO<int> * m_fifo;
	boost::thread m_thread;

	void work() {
		for(int i = 0; i < SIZE_OF_QUEUE; i++) {
			try{
				m_fifo->push(i);
			} catch(MyFIFOException e) {
				printf("Writer exception thrown");
			}
		}
	}
	
	public:
	Writer(MyFIFO<int> * fifo) {
		m_fifo = fifo;
		boost::thread m_thread(&Writer::work, this);
	};
	
	void start() {
		m_thread.detach();
	}

};

BOOST_AUTO_TEST_CASE(ReaderAndWriter) {

	MyFIFO<int> * fifo = new MyFIFO<int>();
	
	Reader reader(fifo);
	Writer writer(fifo);

	reader.start();
	writer.start();

	boost::this_thread::sleep_for(boost::chrono::seconds{1});

	fifo->dump();
}

BOOST_AUTO_TEST_CASE(WriterThenReader) {

	MyFIFO<int> * fifo = new MyFIFO<int>();

	Reader reader(fifo);
	Writer writer(fifo);

	writer.start();
	reader.start();

	boost::this_thread::sleep_for(boost::chrono::seconds{1});

	fifo->dump();
}

BOOST_AUTO_TEST_CASE(ReaderAndWriterAgain) {

	MyFIFO<int> * fifo = new MyFIFO<int>();

	Reader reader(fifo);
	Writer writer(fifo);

	reader.start();
	writer.start();

	boost::this_thread::sleep_for(boost::chrono::seconds{1});

	fifo->dump();
}

