#ifndef MYFIFO_H
#define MYFIFO_H
#define DEBUG

#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <exception>
class MyFIFOException: public std::exception {
  virtual const char* what() const throw()
  {
    return "My exception happened";
  }
};

template <typename T>
class MyFIFO {
	
	static const int SIZE_OF_QUEUE = 5;
	T m_queue[SIZE_OF_QUEUE];
	T * m_writePointer;
	T * m_readPointer;
	T * m_overflowPointer;
	T * incrementPointer(T * ptr, int size);
	int count = 0;

	public:
	MyFIFO();
	~MyFIFO();
	T pop();
	T pop_try();
	void push(T item);
	
	#ifdef DEBUG
	void dump();
	T get(int idx);
	#endif
};

template class MyFIFO<int>;

#endif
