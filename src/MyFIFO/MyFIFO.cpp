#include "MyFIFO.h"
#include <stdio.h>
#include <boost/thread.hpp>

#define SIZE_OF_INT sizeof(int)
static boost::mutex m_mutex;
static boost::condition_variable m_condition;
static bool queueHasItems = false;

template <typename T>
MyFIFO<T>::MyFIFO() {
	m_writePointer = m_queue;
	m_readPointer = m_queue;
	for(int i=0; i<SIZE_OF_QUEUE; i++){
		m_queue[i] = 0;
	}
	m_overflowPointer = &m_queue[SIZE_OF_QUEUE-1];
};

template <typename T>
MyFIFO<T>::~MyFIFO() {}

template <typename T>
T MyFIFO<T>::pop() {
	printf("popping\n");
	boost::mutex::scoped_lock lock(m_mutex);
	while (!queueHasItems && count > 0) {
		printf("waiting\n");
		m_condition.wait(lock);
	}
	printf("resuming\n");
	count--;
	T item = *m_readPointer;
	m_readPointer = incrementPointer(m_readPointer, sizeof(item)/SIZE_OF_INT);
	return item;
};

template <typename T>
T MyFIFO<T>::pop_try() {
	if(count == 0) {
		throw MyFIFOException();
	} else {
		count--;
		T item = *m_readPointer;
		m_readPointer = incrementPointer(m_readPointer, sizeof(item)/SIZE_OF_INT);
		return item;
	}
};

template <typename T>
void MyFIFO<T>::push(T item) {
	printf("pushing\n");
	if(count == SIZE_OF_QUEUE) {
		throw MyFIFOException();
	}
	count++;
	*m_writePointer = item;
	m_writePointer = incrementPointer(m_writePointer, sizeof(item)/SIZE_OF_INT);
	queueHasItems = true;
	printf("notifying\n");
	m_condition.notify_one();
};

template <typename T>
T * MyFIFO<T>::incrementPointer(T * ptr, int size) {
	if(ptr != m_overflowPointer) {
		ptr += size;
	} else {
		ptr = m_queue;
	}
	return ptr;
}

#ifdef DEBUG
template <typename T>
void MyFIFO<T>::dump() {
	printf("| ");
	for(int i=0; i<SIZE_OF_QUEUE; i++) {
		printf("%d, ", m_queue[i]);
	}
	printf("| \n");
}

template <typename T>
T MyFIFO<T>::get(int idx) {
	return m_queue[idx];
}
#endif
